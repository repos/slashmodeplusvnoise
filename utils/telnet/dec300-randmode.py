#!/usr/bin/env python

from telnetlib import Telnet
import random, time

# class to talk to a 'REMOTE' port of a decserver
class dec(Telnet):
        # ansi prefix
        def ansicmd(self,cmd):
                self.write("\x1b[%s" % cmd)
        # ansi goto
        def goto(self,x,y):
                self.ansicmd("%d;%dH" % (y,x))
        # setup terminal
        def init(self):
                self.ansicmd("2J")  # clear screen
                self.ansicmd("3g")  # clear tabs
                self.ansicmd("?7l") # clear autowrap
                self.ansicmd("?1h") # clear cursor
                self.ansicmd("?4l") # smooth scrolling
                self.ansicmd("?25l") # no cursor!

t = dec('192.168.1.103', 2002)
t.init()


def VTprint(str):
	x = 1
	for chr in str:
		t.goto(x,y)
		t.write(chr)
		x = x + 1

y = 1

modes = ["?7m","?5m","?4m","?2m","?1m"]

pool = ["are you ready kids?",
	"aye aye captain!",
	"whoooooooooooooooooooo",
	"lives in a pineapple under the seaaaaa?",
	"SPONGE BOB SQUARE PANTS"]

screen = ["?5l", "?5h"]

while True:
	for stuff in pool:
		t.ansicmd("?K")
		t.ansicmd("?0m")
		t.ansicmd(random.choice(modes))
		VTprint(stuff)
		y = y%24 + 1
		#time.sleep(0.1)
	t.ansicmd(random.choice(screen))

