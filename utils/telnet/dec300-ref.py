#!/usr/bin/env python

from telnetlib import Telnet
import random, time

# recipe:
# setup MOPD en zorg dat je SH1601ENG.SYS in je mop-dir hebt staan
# setup DECSERVER:
# http://wolfuk.net/tscfg.html (password = 'system')
# (evt http://archive.netbsd.se/?ml=sunhelp-rescue&a=2003-11&t=186379 )
# setup a port:
# change port 2 stop bits 1
# change port 2 access remote
# change port 2 break remote
# change port 2 speed 19200
# change port 2 autobaud disabled
# change port 2 autoprompt disabled
# change port 2 broadcast disa
# change telnet listener 2002 port 2
# change telnet listener 2002 connection enabled
# change telnet listener 2002 identification "terminal nummer 2"
# daarna fijn ANSI over telnet tegen die poort aan kletsen, zie onderstaand voorbeeld
# pas op, als je dit met poort 1 doet, kun je hem dus niet meer configureren!!
# oh het apparaat reset je door de rode knop ingedrukt te houden tijdens het gehele bootgedoe

# class to talk to a 'REMOTE' port of a decserver
class dec(Telnet):
        # ansi prefix
        def ansicmd(self,cmd):
                self.write("\x1b[%s" % cmd)
        # ansi goto
        def goto(self,x,y):
                self.ansicmd("%d;%dH" % (y,x))
        # setup terminal
        def init(self):
                self.ansicmd("2J")  # clear screen
                self.ansicmd("3g")  # clear tabs
                self.ansicmd("?7l") # clear autowrap
                self.ansicmd("?1h") # clear cursor
                self.ansicmd("?4l") # smooth scrolling
                self.ansicmd("?25l") # no cursor!

t = dec('192.168.1.103', 2002)
t.init()
while True:
        x = random.randint(1,80)
        y = random.randint(1,24)
        c = random.randint(0,4)
        if c == 0: 
                chr = ' '  
        else:
                i = (x + y) % 4
                chr = 'frop'[i]
        t.goto(x,y)
        t.write(chr)

