SynthDef(\drone, {arg out=0, t_trig=1, freq=440, amp=0.3;
	var env, snd, foldctrl, line;

	//foldctrl = MouseX.kr(0, 1);
	foldctrl = SinOsc.ar(freq / 128 );
        //line = Sweep.kr(Impulse.kr(0.1), 0.1)*1000 + 20;
	//line = XLine.kr(20, 1000, 10);
	//line = Sweep.kr(Impulse.kr(0.1), 0.1)*1000 + 20;
	//line.poll;
	freq = ((freq / 10000) + 1).poll;
	snd = Pan2.ar(Klang.ar(`[Array.rand(12, 20.0, 2000.0), nil, nil ], 1, 0), 1.0.rand);
	env = EnvGen.kr(Env.sine(1), 1, 0.02, doneAction: 0);
	Out.ar(out, snd * env * amp);
}).store;

a = BotSynth2.new(s, botSynthName:"bs1", synthDefName:"drone");
a.synth(\drone);
a.pattern = "this is a test&*@(()280ABFJIZXW".ascii;
a.pattern = "z_.zA._____)".ascii;

a.pattern = "z____A______________________".ascii;
a.pattern = "zfA,dkljdnld".ascii

SynthDef(\drone, {arg out=0, t_trig=1, freq=440, dcy=0.3, amp=0.3;
	var env, snd, foldctrl, line;
	var freqArr, freqOffset=0, freqScale=1.2;
	
	foldctrl = MouseX.kr(0, 1);
	foldctrl = SinOsc.ar(freq / 10000 );
        //line = Sweep.kr(Impulse.kr(0.1), 0.1)*1000 + 20;
//        TChoose.kr(t_trig, [1, 0.25, 0.125, 0.375, 2])
	line = (Sweep.kr(Impulse.kr(1)) + 1).round(1).poll;
	// freqScale = line;
	// env = EnvGen.kr(Env.sine(8), t_trig, 0.02, doneAction: 0).fold2(foldctrl);
	env = EnvGen.kr(Env.perc(0.001, dcy), t_trig, 1, doneAction: 0);
	freqArr = (Array.rand(12, 24, 72).round(3)).midicps;
	snd = DynKlang.ar(`[freqArr, nil, nil], freqScale, freqOffset+line);
	snd = Pan2.ar(snd, LFNoise2.kr(1));
	snd = FreeVerb.ar(RLPF.ar(snd, freq, 0.4), 0.5, 10, 0.02);
	// snd = snd.fold(-1, foldctrl);
	//line = XLine.kr(20, 1000, 10);
	//line.poll;
	Out.ar(out, snd * env * amp * 0.25);
}).store;


(
10.do{|i|
	SynthDef(\drone++i, {arg out=0, t_trig=1, freq=440, transpose=3, dcy=3, amp=0.3;
		var env, snd, foldctrl, line;
		var freqArr, freqOffset=0, freqScale=1;	
		line = (Sweep.kr(Impulse.kr(0.25) * 0.5) + 1).poll; // .round(1).poll; // use as line for ffreq
		env =  EnvGen.kr(Env.perc(0.001.rrand(0.1), dcy), t_trig, doneAction: 0);
		freqArr = (Array.rand(12, 24, 99).round(TChoose.kr(t_trig,[3,7,12]))+transpose).midicps;
		snd = DynKlang.ar(`[freqArr, nil, nil], freqScale*((line*0.25).round(1) + 1), freqOffset) * 0.1;
		snd = snd * env;
		snd = 0.5 * snd + SinOsc.ar((36+transpose).midicps);
		snd = Pan2.ar(snd, LFNoise2.kr(1));
		snd = FreeVerb.ar(RLPF.ar(snd, (freq.lag(0.16)).max(160), 0.2.rrand(0.7)), 0.5, 1, 0.02);
		Out.ar(out, snd * amp);
	}).store;
})

a = BotSynth2.new(s, botSynthName:"bs1", synthDefName:"drone");

a.synth(\drone1);
a.synth(\drone2);
a.synth(\drone3);
a.synth(\drone4);
a.synth(\drone5);

a.pattern = "this is a test&*@(()280ABFJIZXW".ascii;
a.pattern = "()dkdk)".ascii;
a.pattern = "z_.zA._____)".ascii;
a.pattern = "z_.zA._____)".ascii;
// dronish
a.pattern = ".".ascii;
a.sy.set(\dcy, 0.3, \transpose, 0)
a.sy.set(\dcy, 3, \transpose, -2)
a.sy.set(\dcy, 0.03, \transpose, 3)

a.pattern = ".A...............zzzzzz".ascii;
