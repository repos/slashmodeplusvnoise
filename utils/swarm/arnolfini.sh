#!/bin/bash
# script that starts all the craftivism bots

(~/Desktop/mode+v/bots/bezBot.py)&
sleep 5
(~/Desktop/mode+v/bots/drumbot.py)&
sleep 5
(~/Desktop/mode+v/bots/fleabot.py)&
sleep 5
(~/Desktop/mode+v/bots/melodybot.py)&
sleep 5
(~/Desktop/mode+v/bots/strangerbot.py)&
sleep 5
(~/Desktop/mode+v/bots/timebot.py)&
sleep 5
(~/Desktop/mode+v/bots/hungoverbot.py)&
sleep 5
(~/Desktop/mode+v/bots/boredbot.py)&
sleep 5
(~/Desktop/mode+v/bots/enyaBot.py)&
