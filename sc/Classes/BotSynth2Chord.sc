// for chords

BotSynth2Chord {
	classvar <>botSynths, <masterClock, <>shfl=0, <syncTask, tresp, <>restSize=15;
	var <>verbose=false, <>barLength=8, <>stepSynC=true, <>pattern, <>pmod = 127, <>transp=0;
	var <pattlength, <>botPid, <irssiPid, <amp=0.4, <server, <clock, <storedTempo=110, <sy, <proc, <task, <oscTag;
	var <argSynthDefName="fm1", synthControlNames, procControlNames, <procDefName;
	var group, resp, cond, testForKeywords=true;
	var <>botPath;
	
	// class methods
	*initClass {
		botSynths = ();
		masterClock = TempoClock(1);
		masterClock.permanent_(true);
		{
			7.yield;
			Post << nl << "............................................" << nl << "... receiving OSC at port: " << NetAddr.langPort << nl << "............................................" << nl << nl;
		}.r.play;

		this.tempoListen;

	}
	
	*new {|server, botSynthName, synthDefName="fm1"|
		^super.new.init(server, botSynthName, synthDefName) 
	}

	*sync {|stepSync, tempo|
		botSynths.do{|bs| bs.sync(stepSync, tempo) }
	}

	*autosync {|bool=true, sSync=true, tempo=110|
		if(bool){
			if(syncTask.notNil){ syncTask.stop };
			
			syncTask = Task({
				loop{
					BotSynth2.sync(sSync, tempo);
					"\nsynced".postln;
					30.wait;
				}
			});

			syncTask.play;
		}
		{ syncTask.stop }
	}

	*tempoListen {
	
		if( tresp.notNil ){
			tresp.remove;
			this.tempoListen
		}{
			tresp = OSCresponderNode(nil, "/tempo", {|t,r,msg|
				botSynths.do{|bs|
						bs.clock.tempo = msg[1] / 60;
				};

				masterClock.tempo = msg[1] / 60;
			}).add
		}
	}
	
	// instance methods
	init {|serv, myBotSynthName, synthDefName|

		myBotSynthName = "/" ++ myBotSynthName;

		group = Group.new;
		cond = Condition.new;
		server = serv ? Server.default;
		server.boot;
		oscTag = myBotSynthName.asSymbol;

		clock = masterClock;
		// clock = TempoClock.new(storedTempo); 
		pattern = [46, 95, 95, 95, 95, 95, 95, 95];
		pattlength = pattern.size;

		// [70,60,0,0,71,63,0,0,0].sputter(0.25,rrand(3,12));
		// pattern = [70,60,0,0,71,63,0,0,0].sputter(0.25,rrand(3,12));
		// .scramble.pyramid(rrand(1,3))
		// Post << nl << nl << "NetAddr.langPort: " << NetAddr.langPort << nl << nl;

		this.oscResp(oscTag);
	
		Routine.run{
			server.bootSync(cond);
			this.loadDefs(cond);
			server.sync(cond);
			this.synth(synthDefName);
			server.sync(cond);
			this.reset(stepSynC);
		};

		botSynths.put(oscTag, this);
		CmdPeriod.add({ this.free });
		Post << "botsynth inited  " << "name:" << myBotSynthName << " synthdef:" << synthDefName << nl;
	}
	
	startPyBot {|botNickName, botSynthName, commander1, commander2, botpath="minibot.py"|
		var cmd, doit;

		if(commander1.isNil){
			botPath = botPath ? "minibot.py";

			cmd = "~/Desktop/mode+v/bots/minibot.py " ++ botNickName ++ " " ++ botSynthName ++ " " ++ NetAddr.langPort;
			cmd.postln;
		}{ 
			if(botpath.notNil){
				botPath = botpath;
				cmd = "~/Desktop/mode+v/bots/" ++ botPath ++ " " ++ botNickName ++ " " ++ botSynthName ++ " " ++ NetAddr.langPort ++ " " ++ commander1 ++ " " ++ commander2;
				cmd.postln;
			}{
				botPath = botPath ? "monibot.py";
				cmd = "~/Desktop/mode+v/bots/monibot.py " ++ botNickName ++ " " ++ botSynthName ++ " " ++ NetAddr.langPort ++ " " ++ commander1 ++ " " ++ commander2;
				cmd.postln;
			}
		};
		
		// cmd = "exec `xterm -e " ++ cmd ++ "`";
		
		// exec `xterm +u8 -T $i -geometry 80x46+$OFFSET+$OFFSET -e "sleep $DTIME; exit; bash" &`
		// ?? 
		cmd = "exec `xterm +u8 -e " ++ cmd ++ "`";

		doit = { botPid = cmd.unixCmd };
	
		("started: " ++ botPath).postln;
	
		if(botPid.notNil){
			("kill -9 " ++ botPid).unixCmd;
			doit.value;
		}{
			doit.value;
		}
	}
	
	openChatWindow {|nickname="master"|
		var cmd;
		if(irssiPid.notNil){ "kill -9 irssiPid".unixCmd };
		cmd = "exec `xterm -e irssi -c irc.gosub10.org "; 
		cmd = cmd ++ "-n " ++ nickname ++ "`";
		cmd.postln;
		irssiPid = cmd.unixCmd;
	}
	
	
	oscResp {|myBotSynthName|

		if( resp.notNil ){
			resp.remove;
			this.oscResp
		}{
			resp = OSCresponderNode(nil, myBotSynthName, {|t,r,msg|

				if(verbose){ Post << "incoming: " << msg << nl };
				
				// ss bs _ stepsync / barsync if 2nd character is 's'
				if(msg[2] == 115){
					if(msg[1] == 115){ 
						// stepsync if 1st character is 's'
						msg = msg.drop(1).drop(1);
						this.sync(true);
						Post << "stepsync: " << msg;
					}{
						// barsync if 1st character is 'b'
						if(msg[1] == 98){
							msg = msg.drop(1).drop(1);
							this.sync(false.post);
							Post << "barsync: " << msg;
						}{
							//// test for synth messages, todo: tempo
							//if(this.patternToString(msg) == "synth:"){
							//	Post << msg << "\tnot yet implemented" << "!";
							//};	
						}
					}
				}{					
					// [97,109,112, number(0-9) ] amp1 <-> amp9
					if( msg[1] == 97 and:{ msg[2] == 109 and: { msg[3] == 112 } } )
					{
						/*
						"amp: ".post;
						amp = (((msg[4]).ascii - 48) * 0.1)[0];
						sy.set(\amp, amp.postln);
						*/
					}{
						// msg = msg.copyToEnd(1);
						// pattern = msg;
						pattern = msg.copyToEnd(1);

                                                Post << oscTag << " " << argSynthDefName << " " << pattlength << ": " << pattern << nl;

//////////////////////////////////////////////////////////////////// chord hack is here


						pattern = [pattern, (95! restSize)! restSize].flop.flat;
						

////////////////////////////////////////////////////////////////////
							
						pattlength = pattern.size;					
					};
					
				}

			}).add;
		};
		
		oscTag = myBotSynthName;
	}
	
	dumpOSC	{|bool=true, once=false|
		//origOSCReceiveFunc = thisProcess.recvOSCfunc ?? is empty ? okay then leave it alone
		if(bool){
			thisProcess.recvOSCfunc = {|time, replyAddr, msg| 
				if( not(msg[0] == "status.reply".asSymbol)){ //[time,replyAddr,msg].postln;
					msg.postln;
					// if once is 1, then stop dumping after one packet received 
					if(once){ this.dumpOSC(false) };
				}
			};
			NetAddr("127.0.0.1", NetAddr.langPort).sendMsg("/test", 0, 99)
		}{
			thisProcess.recvOSCfunc = {}
		};
	}
	
	/*	// change pattern
	filter {|find, replace|
		pattern = pattern.collect{|item,i| item.replace(find, replace) };
		^pattern
	}
	*/

	sync {|stepSync, myNewTempo, argClock|
		stepSync = stepSync ? stepSynC;
		stepSynC = stepSync;
		
		if(myNewTempo.notNil){ 
			storedTempo = myNewTempo;
		};

		// clear the scheduler, and start a new process 
		// (syncing either to the given clock, or to a new clock)
		// if(clock.notNil){ clock.clear };

		if( clock.notNil ){ clock.clear };
		if( argClock.notNil){ clock = argClock }
		{ 		
			//	<all, <>default;
			//
			//	var <queue, ptr;
			//	var <beatsPerBar=4.0, barsPerBeat=0.25;
			//	var <baseBarBeat=0.0, <baseBar=0.0;
			//	var <>permanent=false;
			// 	*new { arg tempo, beats, seconds, queueSize=256 }		
			clock = TempoClock.new(storedTempo) 
		};
		
		this.run(stepSync, myNewTempo)
	}

	waitForSync {|stepSync=true, tempo| // for the syncmaster
		OSCresponderNode(nil, "/sync0", {|time,rsp,msg|
			if(clock.notNil){ clock.stop };
			clock = TempoClock.new;
			this.run(stepSync, msg[1] ? tempo);
			"just received a syncmaster sync signal".postln;
		}).add.removeWhenDone
	}
	

	///////////////////////// r3340 start / stop methods
	// check: does it realy kill the python at botPid ?
	// otherwise of course clutter of channel with bots

	/*
	startBot {|botPath, botName, channel, ports|
		var command;
		// botPid = this.startBot(botPath, botName, channel, ports);
		botPath = botPath ? "~/Desktop/mode+v/bots/r3340/r3340.py".standardizePath;
		botName = botName ? "r33401-" ++ Date.seed;
		channel = channel ? "#mode+v"; 
		ports = ports ? "57120,57121,57122";

		if(botPid.notNil){ this.killBot; 4.wait };

		command = botPath ++ " " ++ botName ++ " " ++ "\\" ++ channel ++ " " ++ ports;
		// botPid = command.unixCmdGetStdOut;
		botPid = command.unixCmd;
	}
	
	killBot {
		var command;
		command = "kill" ++ " " ++ botPid;
		command.unixCmd;
	}
	*/

	synth {|input|
		var synthDefName, synthFunction;

		if(sy.notNil){sy.free};
		if( input.isKindOf(String) or: { input.isKindOf(Symbol) } )
		{
			synthDefName = input ? argSynthDefName ? "fm1";
		    argSynthDefName = synthDefName;
		    sy.set(\amp,0);
			
			server.waitForBoot({
				if(sy.notNil){ sy.free };
				server.sync(cond);
				sy = Synth(synthDefName.asSymbol, target: group);
				server.sync(cond);
			})
		}{		
			if(input.isKindOf(Function)){
				server.waitForBoot({	
					synthFunction = input ? { |freq| SinOsc.ar(freq) };
					
					synthDefName = \wrapped ++ 10000.rand;

					SynthDef(\synthDefName, { |out|
						Out.ar(out, SynthDef.wrap( synthFunction ))
					}).store;

					sy.free;
					server.sync(cond);
					sy = Synth(synthDefName, target: group);
					server.sync(cond);
				})
			}{
				"needs testing ...".error;
				/*	if(input.isKindOf(NodeProxy)){
					server.waitForBoot({
						synthFunction = input.source;
						sy.free;
						server.sync(cond);
						sy = input.play();
						server.sync(cond);
					});
				
					argSynthDefName = synthDefName ? ("??" ++ 1000.rand);

				}{ 
				};
				*/
				// "no valid input,\nonly synthdefnames and synthfunctions are valid".warn 
			}			
		};

		//s.sync;

		if(verbose){ server.queryAllNodes };

		synthControlNames = SynthDescLib.at(synthDefName.asSymbol).controlNames;
		" ".postln;
		synthControlNames.postln;
		" ".postln;
	}


	process { |input|
		var procFunction;
		
		if( proc.notNil){ proc.set(\amp,0); proc.free; };

		{
		server.bootSync(cond);
		if( input.isKindOf(String) or: { input.isKindOf(Symbol) } )
		{	   
			procDefName = input.asSymbol;

		}
		{
			if(input.isKindOf(Function)){
				procFunction = input;
				procDefName = \procwrapped ++ 10000.rand;
				SynthDef(procDefName, { |in, out|
					Out.ar(out, SynthDef.wrap( procFunction, prependArgs:[in] ))
				}).store;

			}
			{
				// no argument given: just put a reverb
				procDefName = \procverb;				
				procFunction = { |in=0, room=0.7, damp=0.5, mix=0.2|
					FreeVerb.ar(In.ar(in), mix, room, damp) 
				};

				SynthDef(\procverb, {|in=0, out=0, room=0.7, damp=0.5, mix=0.2|
 					Out.ar(out, Pan2.ar(SynthDef.wrap( procFunction, prependArgs:[in,room,damp,mix] ), 0.2*SinOsc.kr))
				}).store;
				server.sync(cond);

				"no valid input; using default process".warn

			}
		};

		server.sync(cond);
		proc = Synth(procDefName, target:group, addAction:\addToTail);
		server.sync(cond);
		}.r.play;

		( SynthDescLib.at(procDefName.asSymbol) ).postln;
		procControlNames = SynthDescLib.at(procDefName.asSymbol).controlNames;

		"now processing".postln; " ".postln;
		procControlNames.postln; " ".postln;		
	}
	
	controlNames {
		Post << nl << "synth: " << synthControlNames << nl << "proc:  "  << procControlNames << nl << nl;
	}
	
	run {|stepsync=true, argTempo|
 		var ii, jj, delta=0.25, step=0, waitForSyncTime;
		
		storedTempo = argTempo ?? storedTempo;
		clock.tempo = storedTempo / 60;
		
		stepsync = stepsync ? stepSynC;
		
		waitForSyncTime = (clock.beats.roundUp - clock.beats);
		// Post << nl << "waitForSyncTime: " << waitForSyncTime;
		
		clock.sched( waitForSyncTime, {


			pattlength = pattern.size;
			jj = step % pattlength;
			
			if( stepSynC ){
				// stepsync
				if( shfl.notNil ){
					if( step%2==0 ){ delta = 0.25 + shfl }{
						delta = 0.25 - shfl
					}
				}{ delta=0.25 }
			}{	
				// barsync
				delta = (clock.tempo.reciprocal  * barLength) / pattlength
			};
			
			server.bind{
				if( not(pattern[jj] == 95) and: { not(pattern[jj] == 32) })
				{
					sy.set(\t_trig, 1,\freq, (((pattern[jj]) % pmod) + transp).midicps);
				}
			};
			step = step + 1;
			delta
		});

		/*	
		Post << oscTag << " running..." << nl << "stepsync: " << stepsync << nl <<
			"clock.tempo: " << clock.tempo << nl;
		*/

	}

	/*
	run2 {|stepsync=true, argTempo=126|
 		var jj, delta=0.25, step=0, barSyncDelta;
		
		// iterate over patternlist (which was filled by the bot)
		task = Tdef(oscTag.asSymbol, {
			inf.do{|count|
				jj = count % pattern.size;
				barSyncDelta = clock.tempo.reciprocal  * 8 / pattern.size;

				if(pattern.notNil){
					if(verbose){
						if( count % pattern.size == 0){"".postln}{" ".post};
						if(pattern[jj] == 32){ Post << "_"; }
							{
								server.bind{ 
									sy.set(\t_trig, 1,\freq, (post(pattern[jj])).midicps) 
								}
							}
					}{
						if(pattern[jj] == 32){/* rest */} 
							{ server.bind{ sy.set(\t_trig, 1,\freq, (pattern[jj]).midicps); } }
					}
				};
				// stepsync
				if(stepsync){ 0.25.wait}
				// barsync
				{					
					if(verbose){ barSyncDelta.post };
					barSyncDelta.wait
				}
			}
		}).play(clock, true, [16,0])
	}

	*/

	reset { this.run; ("reset:" ++ oscTag).postln }

	/*	stop {
		stop sequence, but not clock
	}
	*/

	free {
		clock.stop;
		clock.clear;
		resp.remove;
		botSynths.removeAt(oscTag);
		sy.free;
		group.free;
		// this.killBot;
	}
	
	patternToString {|pattern|
		var string="";
		if(pattern.isKindOf(Array))
		{
			pattern.do{|item, i|	
				string = string ++ item.asAscii;
			};
			^string
		}{
			"input not an Array".warn
		}
	}

	loadDefs {|synthDefName, cond|

		// simple sequencer sine
		SynthDef(\sssine,{ |out=0, t_trig=1, freq=440, dcy=0.2, amp=0.9|
			var env, snd;
			env = EnvGen.ar(Env.perc(1e-3,dcy), t_trig);
			snd = SinOsc.ar(freq*[1,1.01]);
			OffsetOut.ar(out, snd * env * amp);
		}).store;

		/*
		Synth(\sssine,[\freq, 4000, \dcy, 2])
		*/

		// simple sequenser pulse
		SynthDef(\sspulse,{ |out=0, t_trig=1, freq=200, width=0.5, dcy=0.1, amp=0.6|
			var env, snd;
			env = EnvGen.ar(Env.perc(1e-2,dcy), t_trig);
			snd = Pulse.ar(freq!2, width);
			OffsetOut.ar(out, snd * env * amp * 0.5);
		}).store;


		/*
		Synth(\sspulse,[\freq, 1000, \width, 0.02, \dcy, 2])
		Synth(\sspulse,[\freq, 250, \width, 0.1, \dcy, 2])
		*/

		// ssfm
		SynthDef(\ssfm, {| outBus=0, t_trig=1, freq=1040, modfreq=100, modindex=1, dcy=0.2, amp=0.3|
			var snd, env = EnvGen.ar(Env.perc(0.01, dcy ), t_trig, doneAction:2);
			snd = PMOsc.ar(freq, modfreq, modindex,[0, SinOsc.kr(0.3,0,pi)]);
			OffsetOut.ar(outBus, snd * env * amp);
		}).store;


		/*
		Synth(\ssfm,[\freq, 1250, \modfreq, 1.5, \modindex, 5, \dcy, 2, \amp, 1])
		Synth(\ssfm,[\freq, 150, \modfreq, 200.5, \modindex, 10, \dcy, 2, \amp, 1])
		*/

		// ssfilterednoise
		SynthDef(\ssfnoise, {| outBus=0, t_trig=1, freq=1000, rq=0.1, dcy=0.2, amp=0.3|
			var snd, env = EnvGen.ar(Env.perc(0.001, dcy ), t_trig, doneAction:2);
			snd = BPF.ar(WhiteNoise.ar(4), freq, rq); // freq is filter cutoff frequency here, rq is the reciprocal of q (bandwith of filter)
			OffsetOut.ar(outBus, snd!2 * env * amp);
		}).store;


		/*
		Synth(\ssfnoise, [\freq, 2000, \rq,0.001, \dcy, 0.9, \amp, 1]);
		*/

		// ssfilterednoise2   
		SynthDef(\ssfnoise2, {| outBus=0, t_trig=1, freq=1000, rq=0.1, dcy=0.2, fdcy=0.04, amp=0.3|
			var snd, fenv, env = EnvGen.ar(Env.perc(0.001, dcy ), t_trig, doneAction:2);
			fenv = EnvGen.ar(Env.perc(1e-5, fdcy, curve:-4), t_trig, doneAction:0);
			snd = BPF.ar(WhiteNoise.ar(4), freq*(1+fenv), rq); // freq is filter cutoff frequency here, rq is the reciprocal of q (bandwith of filter)
			OffsetOut.ar(outBus, snd!2 * env * amp);
		}).store;

		/*
		Synth(\ssfnoise2, [\freq, 4000, \rq,0.1, \dcy, 0.1, \fdcy, 0.03, \amp, 3]);
		*/

		SynthDef(\honky,{ |out=0, t_trig=1, freq, dcy=0.1, amp=0.9|
			var snd, env = EnvGen.ar(Env.perc(1e-3,dcy), t_trig);
			snd = SinOsc.ar(freq!2).cubed.cubed;
			OffsetOut.ar(out, snd * amp * env);
		}).store;


		SynthDef(\drum1, {|out=0, t_trig=1, freq, freqMul=4, dcy=0.1, fdcy=0.02, rq=0.2, amp=0.7|
			var snd, fenv, env = EnvGen.ar(Env.perc(1e-5, dcy, curve:-8), t_trig);
			fenv = EnvGen.ar(Env.perc(1e-5, fdcy, curve:-4), t_trig, doneAction:0);
			snd = BPF.ar( WhiteNoise.ar(10) ! 2, freq * freqMul * fenv + 100, rq);
			OffsetOut.ar(out, snd.softclip * amp * env);
		}).store;

		/*
		Synth(\drum1, [\freq, 1500, \freqMul, 10, \dcy, 0.6, \fdcy, 0.3, \amp, 3]);
		*/

//		SynthDef(\drum2, {|out=0, t_trig=1,freq, freqMul=4, dcy=0.1, rq=0.1, amp=0.4|
//			var snd, env = EnvGen.ar(Env.perc(1e-5, dcy, curve:-8), t_trig);
//			snd = BPF.ar( WhiteNoise.ar(10)!2, freq * freqMul + 100, rq) 
//			+ SinOsc.ar(freq/2)!2;
//			OffsetOut.ar(out, snd * amp * env * 0.3);
//		}).store;

		SynthDef(\drum2, {|out=0, t_trig=1,freq, freqMul=4, dcy=0.1, ffreq=1000, rq=0.3, amp=0.4|
			var snd, env = EnvGen.ar(Env.perc(1e-9, dcy, curve:-8), t_trig);
			snd = BPF.ar( WhiteNoise.ar(10)!2, freq * freqMul + 100, rq) 
			+ SinOsc.ar(freq/2)!2;
			OffsetOut.ar(out, LPF.ar(snd, ffreq, amp * env * 0.5));
		}).store;

		SynthDef(\fm1, {| outBus=0, t_trig=0, freq=1040, dcy=0.3, amp=0.4|
			var snd, env = EnvGen.ar(Env.perc(0.01, freq.reciprocal * 200 * dcy ), t_trig, doneAction:0);
			snd = PMOsc.ar(freq, freq*0.5, 2*TExpRand.kr(0.5, 4, t_trig),[0, SinOsc.kr(0.3,0,pi)]);
			OffsetOut.ar(outBus, snd * env * amp);
		}).store;


		SynthDef(\fm2, {| outBus=0, t_trig=0, freq=1040, dcy=0.09, amp=0.6|
			var snd, env = EnvGen.ar(Env.perc(0.01,dcy*LFNoise2.kr(0.1,0.1,1)), t_trig, doneAction:0);
			snd = RLPF.ar(
					PMOsc.ar(freq, freq*[1.5, 2, 1].choose, TRand.kr(0.5, 3, t_trig),0),
					SinOsc.kr([0.1,0.11]).range(400, 1900),
					0.4
			);
			OffsetOut.ar(outBus, snd * env * amp);
		}).store;


		SynthDef(\ana1, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, dcy=0.3, pdcy=0.07,
			ffreq=700, rq=0.3, amp=0.3|
			var aenv, penv, osc1, osc2, filtd;
			aenv = EnvGen.ar(Env.perc(0.01, rrand(dcy/2,dcy*2)), t_trig, doneAction: 0);
			penv = 1 + EnvGen.ar( Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
			pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
			sfreq = freq;
			pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1);
			osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
			osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
			filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
			OffsetOut.ar(0, filtd * aenv * amp * 0.7)
		}).store;


		SynthDef(\ana2, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, dcy=0.3, 
			pdcy=0.07, ffreq=700, rq=0.3, amp=0.4|
			var aenv, penv, osc1, osc2, filtd;
			aenv = EnvGen.ar(Env.perc(0.01, rrand(0.02,dcy)), t_trig, doneAction: 0);
			penv = 1 + EnvGen.ar(Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
			pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
			sfreq = freq  * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
			pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1) * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
			osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
			osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
			filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
			OffsetOut.ar(0, filtd * aenv * amp * 0.5)
		}).store;


	}

}
