#!/usr/bin/perl  

# code by rob canning rob@goto10.org
# licence GNU GPL

#Dependencies:
#http://search.cpan.org/~crenz/Net-OpenSoundControl-0.05/
#http://search.cpan.org/dist/Bot-BasicBot/lib/Bot/BasicBot.pm  - sudo aptitude install libbot-basicbot-perl

package OSCBot;

use base qw(Bot::BasicBot);
use Net::OpenSoundControl::Client;
use warnings;
use strict;

my $client = Net::OpenSoundControl::Client->new(
    Host => "localhost", Port => 7777)
    or die "Could not start client: $@\n";


sub help { "I send messages to an  OSC server on port 7777 on localhost - to send a message to the OSC server prepend your message with \"osc\" - the 
messages are sent in the format /irc/11811/nick/the message " };


sub on_join {}

sub chanjoin {
    my ($self, $message) = @_;
    return "hello i am 11811\n";
}
 

sub said {

    my ($self, $message) = @_;

    if ($message->{body} =~ /\b^osc \b/) {
        
	my $nick = $message->{who};
	
	my $str = $message->{body};
	$str =~ s/osc//g;
	
	my @ascii = unpack("C*", "$str");
	
	
	return {
	    $client->send(["/irc/11811/$nick/@ascii"])
		
	}
    }
}

OSCBot->new(
  
    server => "irc.goto10.org",
    channels => "#mode+v ",
    nick => 'x11811',
    ignore_list => [qw(3340)],
    )->run();
