(~path +/+ "home.scd").openTextFile;




/*
(~path +/+ "config.scd").openTextFile;
// kill jackd
"killall -9 jackd".systemCmd;
// start jackd if it's not running
"/usr/bin/jackd -R -dalsa -dhw:0 -r44100 -p2048 -n2".systemCmd;
"/usr/bin/jackd -R -dalsa -dhw:0 -r44100 -p2048 -n2".systemCmd;
*/

//----------------/* d2 1/----------------------------------------------------

p.pop // leave proxyspace if needed

~path = "~/Desktop/mode+v/workshop/supercollider_tutorial".standardizePath;

// osc
(~path +/+ ".xtra/osc.scd").openTextFile;




//------------------------------------------------------------------- BotSynth
(~path +/+ "mode+v.scd").openTextFile;







//--------------------------------------------------------------- RemoteClient
// change botsynth params while it's playing
// explore controlling your synth remotely

(~path +/+ ".xtra/remoteclient.scd").openTextFile;