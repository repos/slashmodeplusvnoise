(~path +/+ "home.scd").openTextFile;
////

////////////////// proxies 1
- set and triggering with tdef
- processing: filter nodeproxies
////////////////////////////

p = ProxySpace.push(s.boot);
p.clear
////////////////////////////////////////////
// synthdef as proxy input
n = NodeProxy.audio(s,2); // single proxy, no proxyspace needed
n.play

n.source = {SinOsc.ar(440)!2}
n.source = SynthDef("w", { arg out=0; Out.ar(out,SinOsc.ar([Rand(430, 600), 600], 0, 0.2)) });

////




~out2.play;

(
~out2 = { 
	|syncfreq = 50, sawfreq = 50, filterfreq = 100, amp = 0.1|
	RLPF.ar(
		in:	SyncSaw.ar(syncfreq.lag3(1), sawfreq.lag(2)),
		freq: filterfreq.lag2(3),
		rq: 0.2,
		mul: amp
	)!2
}
)
~out2.set(\syncfreq, 110, \sawfreq, 200, \filterfreq, 400)
~out2.set(\syncfreq, 55, \sawfreq, 50, \filterfreq, 500)


//////////////////////////////////////// 
// Tdef = named task, task = routine


// post, postln, postcln
(
Tdef(\whow, {
	inf.do{
		|i|
		~out.set(\syncfreq, (60*i).postln, \sawfreq, i*200+10, \filterfreq, 100);
		wait(2.rrand(7));
		~out.set(\syncfreq, 11, \sawfreq, (10+i*10).postcln, \filterfreq, 5000);
		wait(1)
	}
})
)

Tdef(\whow).play;


/////////////////////////////////////////////////// chains of processing
~out.fadeTime = 1;
~out.set(\amp, 0.09);
~out.clear


~out2[1] = \filter -> {|in|  [in, CombN.ar(in, 1, 0.2, 0.5)]  }
~out2[1] = \filter -> {|in|  [in, CombC.ar(in, 1, LFNoise2.kr(0.2,0.09,0.1), 0.5)]  }
~out2[1] = \filter -> {|in|  [in, CombC.ar(in, 1, LFNoise1.kr(0.8,0.09,0.1), 2)]  }

~out2[3] = \filter ->  {|in|  BPF.ar(in, 1000, 0.2)  }
~out2[3] = \filter ->  {|in|  BPF.ar(in, SinOsc.kr(LFNoise0.kr(0.5,5.8,6)).range(500,3000), 0.4)  }

~out2[4] = \filter ->  {|in|  in * LFPulse.kr(4, width: SinOsc.kr(1.5,0,0.2,0.75))  }
~out2[4] = \filter ->  {|in|  in * Decay.ar(Impulse.ar(12),0.05)  }
~out2[4] = \filter ->  {|in|  in * Decay.ar(Dust.ar(12), LFNoise1.kr(0.2).range(0.01,0.2))  }

//	*ar { arg in, mix = 0.33, room = 0.5, damp = 0.5, mul = 1.0, add = 0.0;
~out2[5] = \filter ->  {|in|  in + FreeVerb.ar(in, damp: 0.1, room: LFNoise2.kr(0.3).range(0.3,0.8))  }

~out2.set(\amp, 1.8)
~out2[2] = \filter ->  {|in|  in.distort  }
~out2[2] = \filter ->  {|in|  in.softclip  }
~out2[2] = \filter ->  {|in|  in.tanh  }

~out2[7] = \filter -> {|in|  in + 0.7 * CombC.ar(in, 3, XLine.kr(3,0.02,30), 7)  }
~out2[7] = \filter -> {|in|  in + 0.7 * CombC.ar(in, 3, XLine.kr(0.02,3,30), 7)  }

// get sound in instead
~out2[0] = { AudioIn.ar([1,2]]) }

Tdef(\whow).stop;

////
~out2.free;

~out.stop
~out2.stop

// p.clear;
p.pop;








//// tdef environment
/*
Tdef(\whow, {|env|
	env.arr = [90, 80, 70, 76].midicps;
	inf.do{
		|i|
		var size = env.arr.size;
		~out.set(\syncfreq, env.arr[i%size], \sawfreq, 200, \filterfreq, 100);
		wait(2);
		~out.set(\syncfreq, env.arr[i%size], \sawfreq, 100, \filterfreq, 5000);
		wait(1)
	}
})

Tdef(\whow).set(\arr, [90, 80, 70, 76].midicps )
Tdef(\whow).set(\arr, [69, 68, 66, 71].midicps )
Tdef(\whow).set(\arr, [69, 65].midicps )


Tdef(\whow).play
Tdef(\whow).stop

*/