(~path +/+ "home.scd").openTextFile;
///

// Patterns and samples 

// execute these codeblocks one after the other, 
// make sure you fill the ~samplePath variables with  paths with .wav files like below


(
s.boot;
// change to your path: do a $locate .wav or $find .wav 
// to find some .wav folders (shorter samples are better)
// outcomment 2and 3 if you want more paths

~samplePath = "/usr/share/kismet/wav/*.wav";
~samplePath2 = "/usr/share/sounds/alsa/*.wav";
~samplePath3 = "/usr/share/sounds/purple/*.wav";

// afunction for loading the samples into buffers
~getBuffers = {|path| 
	fork{
		y = path.pathMatch.collect({ |path|
			var buf;
			buf = Buffer.read(s, path);
			s.sync;
			buf;
		});
	}
};
)


//// load synthdef and playfunctions
(
SynthDef(\playsample, { |bufnum=0, rate=1.0, amp=0.3, pan=0|
	var sig;
	sig = PlayBuf.ar(1, bufnum, rate * BufRateScale.kr(bufnum), loop:0, doneAction:2);
	sig = sig;
	sig = Pan2.ar(sig, pan, amp);
	Out.ar(0, sig);
}).memStore;

SynthDef(\playsample_env, { |bufnum=0, rate=1.0, dcy=0.1, amp=0.3, pan=0|
	var sig;
	var env;
	env = EnvGen.kr(Env.perc(0.001,dcy), 1, doneAction:2);
	sig = PlayBuf.ar(1, bufnum, rate * BufRateScale.kr(bufnum), loop:0);
	sig = sig * env;
	sig = Pan2.ar(sig, pan, amp);
	Out.ar(0, sig);
}).memStore;

//s
// a function choosing a pattern (slow or fast), and playing it
~refresh = {|slow=true|
	if(slow){
	Pdef(\randfast).stop;
	Pdef(\randslow,
		Pbind(\instrument, \playsample,
		\bufnum, Pxrand(y, inf),
		\amp, Pxrand( [0.1, 0.5, 1.0], inf),
		\rate, Pwhite(0.1,1.2),
		\dur, Pxrand(Array.geom(13,0.02,1.2),inf),
		\pan, Pxrand( [-1, -0.5, 0, 0.5, 1], inf)
		)
	).play;
    }
    {
	Pdef(\randslow).stop;
	Pdef(\randfast,
		Pbind(\instrument, \playsample,
		\bufnum, Pxrand(y, inf),
		\amp, Pxrand( [0.1, 0.5, 1.0], inf),
		\rate, Pwhite(0.1,8),
		\dur, Pxrand(Array.geom(13,0.02,1.2),inf),
		\pan, Pxrand( [-1, -0.5, 0, 0.5, 1], inf)
		)
	).play;


	Pdef(\rhythm1,
		Pbind(\instrument, \playsample_env,
		\bufnum, Pxrand(y, inf),
		\amp, Pxrand( [0.1, 0.5, 1.0], inf),
		\rate, Pwhite(1,2),
		\dur, Pxrand(Array.geom(13,0.02,1.2),inf),
		\pan, Pxrand( [-1, -0.5, 0, 0.5, 1], inf)
		)
	).play;




	}
};
)

// lastly evaluate the following two
// get the buffers
~getBuffers.value(~samplePath); 
// play the pattern
~refresh.value(true);

~getBuffers.value(~samplePath2); ~refresh.value(false);
~getBuffers.value(~samplePath3); ~refresh.value(true);

//stop it

Pdef(\randslow).stop;
Pdef(\randslow).play

Pdef(\randfast).play
Pdef(\randfast).stop;


// always release buffers
Buffer.freeAll;

s.quit
