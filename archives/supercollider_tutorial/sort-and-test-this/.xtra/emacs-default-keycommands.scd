(~path +/+ "home.scd").openTextFile;  // go back to main menu
(~path +/+ "emacs-sclang.scd").openTextFile; // go back to emacs menu


////  Emacs default Key Bindings
Lines
    C-a             go to the beginning-of-line
    C-e             go to the end-of-line
    C-n             go to next-line
    C-p             go to previous-line
    C-k             kill the current line
    C-o             open-line

Words
    ESC f           forward-word
    ESC b           backward-word
    ESC d           kill-word
    ESC DEL         backward-kill-word

Characters
    C-f             forward-char
    C-b             backward-char
    C-d             delete-char
    DEL             delete-backward-char
    C-q             quoted-insert
    C-t             transpose-chars

Regions
    C-space         set a region mark
    C-w             kill-region (between cursor and mark)
    ESC-w           memorize the contents of the region (without kill)
    C-y             yank (i.e., insert text last killed or memorize)

Screen control
    C-l             recenter
    C-v             scroll-up (forward)
    ESC-v           scroll-down (backward)
    ESC <           beginning-of-buffer
    ESC >           end-of-buffer

Search
    C-s             isearch-forward
    C-r             isearch-backward

Files
    C-x C-f         find-file
    C-x C-r         find-file-read-only
    C-x C-s         save-current-file

Windows
    C-x 1           delete-other-windows
    C-x 2           split-window-vertically
    C-x 4 f         find-file-other-window
    C-x o           other-window

Command execution
    ESC !           shell-command
    ESC x compile   compile ("make -k" is default)
    C-x `           next-error
                    (used after "compile" to find/edit errors)

Miscellaneous
    C-x C-c         save-buffers-kill-emacs
    C-u             universal-argument
    C-x C-z         suspend-emacs
                    (resume by typing "fg" to unix)

Help!
    C-g             keyboard-quit
    C-h             help-command
    C-h t           help-with-tutorial
    C-h b           describe-bindings (complete list of emacs commands)

//////////////////////////