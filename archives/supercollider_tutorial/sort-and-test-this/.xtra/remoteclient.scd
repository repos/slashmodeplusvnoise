(~path +/+ "home.scd").openTextFile;
(~path +/+ "day2.scd").openTextFile;

// RemoteClient is a wrapper class for Client and LocalClient
// it is used to execute on another machine


// evaluate this on the receiving machine
RemoteClient.listen; // listen for incoming messages

// and this on the client, ie. where you're at. 
RemoteClient.new // address to send to defaults to localhost port:57120

// for controlling a remote sclang interpreter, we make a new client to send code to another computer
// we need the ip and NetAddr.langPort (57120 or 57121)

// ip retrieval
"avaio.local".gethostbyname.asIPString;
"vca.local".gethostbyname.asIPString;
//"hee.local".gethostbyname.asIPString;

RemoteClient("192.168.5.147", 57120);



//----------------------------------------------------------------------------------//
// code is sent as a string
// so you need to escape quotes ( "", '') and slashes ( \, / )
// example: botsynth; 
// change synth and parameters


RemoteClient.send(codeString:"~bzn=BotSynth2(s,\'\/bzn\'); ~bzn.run;");
RemoteClient.send("~bzn.synth(\\anasynth1)")
RemoteClient.send("~bzn.synth(\\anasynth2)") 
RemoteClient.send("~bzn.sy.set(\\ffreq, 300.exprand(4000), \\rq, 0.7.rrand(0.1))")

RemoteClient.send("~bzn.synth(\\fm2)")
RemoteClient.send("~bzn.sy.set(\\dcy, 0.9, \\amp, 0.4)")


// need a recent sc build for .ascii (convert strings to arrays of ascii values)
RemoteClient.send("~bzn.pattern =\"((((----))))----\".ascii") 




//---------------------------------------------------------------------------------//

// the remote server to send a synthdef to (check ip)
r=Server(\hii, NetAddr("192.168.5.147", 57110));

// or local scsynth
r = s;


// send a new synthdef
(
SynthDef(\mySound, {| outBus=0, t_trig=1, freq=1040, dcy=0.1, amp=0.4|
	var snd, env = EnvGen.kr(Env.perc(0.01,dcy), t_trig, doneAction:0);
	snd = Pan2.ar(SinOsc.ar(freq,0,env).cubed.cubed,0);
	Out.ar(outBus,snd * env * amp);
}).send(r);
)


// start a botsynth 
RemoteClient.send(codeString:"~bzn=BotSynth2(s,\'\/bzn\'); ~bzn.run;");

// make it play 'mySound'
RemoteClient.send("~bzn.synth(\\mySound)") 
// change the pattern
RemoteClient.send("~bzn.pattern = \"ak  kAS#@@@777&&&@\".ascii") 

//

(
{
	7.do{ 
	RemoteClient.send("~bzn.pattern = \"akk_d  DS hde fgh ij\".scramble.ascii");
	2.rrand(9).wait;
	}
}.fork
)


(
x = {

	120.do{
		if( 0.2.coin ){ 
			a = String.rand ++ "  " ++ String.rand(8)  ++ "  " ++ String.rand(2) 
		}{ 
			a = "((((----))))----AAA-BBB" 
		};

		RemoteClient.send("~bzn.pattern = a.scramble.ascii");

		2.rrand(9).wait;
	}

}.fork
)

x.stop
x.play


////
a = String.rand ++ "  " ++ String.rand(8)  ++ "  " ++ String.rand(2);
a = "3#  AA A DD  CF"


RemoteClient.send("~bzn.run")
RemoteClient.send("~bzn.synth(\\anasynth2)") 
RemoteClient.send("~bzn.sy.set(\\ffreq, 300.exprand(4000), \\rq, 0.7.rrand(0.1))")


// need a recent sc build for .ascii (convert strings to arrays of ascii values)
a = "((((----))))----AAA-BBB"
RemoteClient.send("~bzn.pattern = a.scramble.ascii") 


// get the objects like this in case you need them
RemoteClient.localClient
RemoteClient.client
