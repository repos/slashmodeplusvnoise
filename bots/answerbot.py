#!/usr/bin/env python
# simple bot that answers when adressed directly

import liblo, sys
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# we are the robots
class TestBot(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_welcome(self, c, e):
        c.join(self.channel)

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) > 1 and irc_lower(a[0]) == irc_lower(self.connection.get_nickname()):
            self.do_command(e, a[1].strip())
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
 
        if cmd == "hello":
            c.notice(self.channel, "hello")
        elif cmd == "cat":
            c.notice(self.channel, "meow")
        else:
            c.notice(self.channel, "hu?")

def main():
    bot = TestBot("#mode+v", "answerbot", "irc.goto10.org", 6667)
    bot.start()

if __name__ == "__main__":
    main()
