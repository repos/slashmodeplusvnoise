#!/usr/bin/env python
# craftivism.hungoverbot.py
# Originally written by Jonny Stutters
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make hungoverbot happy

import liblo, sys, r3340, time, random
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# hungoverbot config

channel = "#mode+v"
nickname = "hungoverbot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
targets = ["melodybot", "rhythmbot", "fleabot", "enyaBot"]
mybotsynth = ""

if len(sys.argv) == 2:
    server = sys.argv[1]

# hungoverbot's guts

class hungover(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
	
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server
        targetId = 0
        tempo = 120
        while 1:
            c = self.connection
            waittime = random.randint(30, 120)
            if random.randint(0,10) == 1 :
                c.privmsg(self.channel, targets[targetId] + ": shut up")
                c.privmsg(targets[targetId], "shut up")
                time.sleep(60)
	        c.privmsg(self.channel, "That's better. I can get some kip for " + str(waittime))
                targetId += 1
                if targetId >= len(targets):
                    targetId = 0
                time.sleep(waittime)
            if random.randint(0,10) == 1 :
                c.privmsg(self.channel, "lunchtime?")
                time.sleep(waittime)           
            if random.randint(0,1) == 1 :
                
                c.privmsg(self.channel, "I feel funny...")
                newtempo = random.randint(60,180)
                steps = float(random.randint(1,100))
                steplength = (newtempo - tempo) / steps
                stepduration = random.random() + 0.0001 
                
                for step in range(steps):
                    tempo += steplength
                    print tempo
                    msg = liblo.Message("/tempo")
                    msg.add(tempo)
                    liblo.send(self.target, msg)
                    time.sleep(stepduration)

                time.sleep(waittime)
 
# hungoverbot go!

hungoverbot = hungover(channel, nickname, server, IRCport, OSCport, mybotsynth)
hungoverbot.start()
