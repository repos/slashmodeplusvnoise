#!/usr/bin/env python
# strangerbot.py
# Originally written by Jonny Stutters
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make strangerbot happy

import liblo, sys, r3340, re
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# strangerbot config

channel = "#mode+v"
nickname = "strangerbot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
mybotsynth = "/bs3"

if len(sys.argv) == 2:
    server = sys.argv[1]

# strangerbot's guts

class strange(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
	
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def on_privmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            cmd = a[0]
            print nickname + " " + cmd
            if re.match("shut up", cmd):
                self.do_shutup(c, e)

    def do_shutup(self):
        msg = liblo.Message(self.mybotsynth)
        msg.add(ord("_"))
        liblo.send(self.target, msg)
        c = self.connection
        c.notice(self.channel, "I'm shutting up")
        r3340.notify(1, "Shutting up")
        
    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        if re.match(nickname + " shut up", cmd):
            self.do_shutup()
        elif nick != "animal" and nick != "flea" and nick != "sting" and nick != "enya" and nick != "slayer" and nick != "bez" and nick != "hungoverbot" and nick != "boredbot" and nick != "melodybot":
            
            nickascii = 0
            for i in nick:
                nickascii = nickascii + ord(i)
            mybotsynth = "/bs" + str(nickascii%6 + 1)
	
            msg = liblo.Message(mybotsynth)
            for i in cmd:
                msg.add(ord(i))
            liblo.send(self.target, msg)
            c = self.connection
            c.notice(self.channel, nick + " told me to do something with " + mybotsynth)
            r3340.notify(1, nick + ':' + cmd + ' sent to ' + mybotsynth)

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)


# strangerbot go!

strangerbot = strange(channel, nickname, server, IRCport, OSCport, mybotsynth)
strangerbot.start()
