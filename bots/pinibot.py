#!/usr/bin/env python
# minibot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# minibot config

channel = "#mode+v"
nickname = "pini"
server = "irc.gosub10.org"
IRCport = 6667
OSCport = 57121
mybotsynth = "/h4nk"

# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        self.modep = 0
        self.patternDict = {
            '4':        '!_!!..@.!_!!..j.!_!!..@.!_!!..j.!_!!..@.!_!!..j.%#^#', 
            'sape':     '4139', 
            '67':       'JKJJKJJKJJKJOPOOPOOPOOPOXYZOPOXYZ29090', 
            '3':        '!_!!..@.!_!!..j.!_!!..@.!_!!..j.', '1': '!_!!..@.', 
            'bass4':    '.3890000....0000....................,,,,,,,,,,,,....0000,,,,....0000,,,,....00998877.
            'bass5':    '3890000....0000....................,,,,,,,,,,,,....0000,,,,....0000,,,,....', 
            '2':        '!_!!..j.', 
            'bass2':    '.3890000....0000....................,,,,,,,,,,,,....0000,,,,....0000,,,,'
        }
        
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        # master gets his message sent to OSC directly...
        if nick == "master":
           msg = liblo.Message(self.mybotsynth)
           for i in cmd:
                    msg.add(ord(i))

           if cmd == "mode+p" :
                self.is_Preset(cmd)
           else:
                if self.modep == 1:
                    self.is_Preset(cmd)
                else: 
                    liblo.send(self.target, msg)
                    r3340.notify(1, "sent: " + cmd)


        # ...but super's messages are scried first.
        elif nick == "super":
            msg = liblo.Message(self.mybotsynth)
            
            string = r3340.cervelle(cmd)
            string.scry(100)

            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)

            r3340.notify(1, nick + ':' + string.message)

        elif cmd == "help":
            c.notice(self.channel, "I'm helpful")

        elif cmd == "yo" and nick == "bob":
            c.notice(self.channel, "I'm helpful, bob")

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)

    def is_Preset(self, cmd):
        c = self.connection
        pdict = self.patternDict
        msg = liblo.Message(self.mybotsynth)
                 
        if(not self.modep):
            r3340.notify(1, "entering mode+p")
            c.notice(self.channel, "entering mode+p")
            c.notice(self.channel, "-load/save patterns: name pattern")
            c.notice(self.channel, "ie. rock01 9999666600003333")
            c.notice(self.channel, "commands: show save load")

        #c.notice(self.channel, "tip: use Control-a to go to the beginning of the line and write pattern name (don't forget one whitespace")
        self.modep = 1

        if cmd == "mode+p":
            5 + 9        
        elif cmd == "show":
            c.notice(self.channel, pdict)

        elif cmd.find("save") == 0:

            cmd = cmd.replace("save ", "")        
            name = cmd.partition(" ")[0]
            patt = cmd.partition(" ")[2]
            command = ""

            patt_array = patt.rsplit(" ")
            if len(patt_array) > 1:
                # check sentence for words/patterns from dict, concatenate the words' patterns
                for word in patt_array:
                    if word in pdict:
                        command = command + pdict[word]
                    else:
                        command = command + word
                # store in dict
                pdict[name] = command
                c.notice(self.channel, command + " -> " + name)
            else:
                pdict[name] = patt
                c.notice(self.channel, patt + " -> " + name)

            for i in pdict[name]:
                msg.add(ord(i))
            liblo.send(self.target, msg)


        # is the cmdstring a key ?
        elif cmd in pdict:            
            c.notice(self.channel, " <- " + pdict[cmd])
            command = pdict[cmd]
            for i in command:
                msg.add(ord(i))
            liblo.send(self.target, msg)

        # maybe there's a key hidden somewhere within the string ?
        else:
            #c.notice(self.channel, " <- " + pdict[cmd])
            command = ""
            patt_array = cmd.rsplit(" ")

            if len(patt_array) > 1:
                # check sentence for words/patterns from dict, concatenate the words' patterns
                for word in patt_array:
                    if word in pdict:
                        command = command + pdict[word]
                    else:
                        command = command + word
                c.notice(self.channel, "composed: " + command)
            else:
                command = cmd

            for i in command:
                msg.add(ord(i))

            liblo.send(self.target, msg)

# minibot go!
minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
