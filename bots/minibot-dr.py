#!/usr/bin/env python
# minibot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr


# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, master):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        self.master = master

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        if nick == self.master:
            msg = liblo.Message("/"+nick)
            
            if cmd == "GO":
                a = r3340.cervelle(cmd)
                a.remix()
                print a.message
            else:
                for i in cmd:
                    msg.add(ord(i))
                
                liblo.send(self.target, msg)
                print '\033[1;32m:) \033[m' + nick + ': ' + cmd

        else:
            print '\033[1;31m:( \033[m' + nick + ':' + cmd


# minibot config

channel = "#mode+v"
nickname = "mini"
server = "localhost"
IRCport = 6667
OSCport = 57120
master = "d"

# minibot go!
minibot = mini(channel, nickname, server, IRCport, OSCport, master)
minibot.start()
