#!/usr/bin/env python
# r3340.01 - an OSC IRC bot that want to take over the world
# DEPENDS: python-liblo, python-irclib
# sends to OSC server <port> on localhost, joins <channel>, as <nickname>
# TODO:

# modes
#   0 serial like now
#   1 empty qeue immediately and send this
#   2 parallel commands

# hmm, tried with threading
# but still one task blocks the nest one untill its finished (gets queued)

# on welcome: get usernames and put them in an array
# send these random messages now and then for some extra activity (get howie too!)
# c.notice(self.channel, "Howie: I am \033[1;31m3340.01\033[m ")


import liblo, sys
import string, time, random
import threading
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

if len(sys.argv) == 4:
    (nickname, channel, ports) = (sys.argv[1], sys.argv[2], sys.argv[3])
else:
    print '\033[1;31mUsage:\033[m r3340.01.py <nickname> \<irc-channel> <OSC port(s)>'
    print '\033[1;32mexample:\033[m ./r3340.py r33401 \#mode+v 57120'
    print '\033[1;32mexample:\033[m ./r3340.py banana \#goto10 9997,9998,9999'
    print 'see also r33401:help'
    sys.exit(1)

try:
    targets = {}
    for port in ports.split(','):
        targets[port] = liblo.Address(port)

except liblo.AddressError, err:
    print str(err)
    sys.exit("oioioi osc address error")

# we are the robots
class R33401(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.message = ",."
        #self.server = server


    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

        c.notice(self.channel, "Hello, I am \033[1;31m3340.01\033[m ")
        c.notice(self.channel, "feed me ASCII and I'll pass it to OSC port(s) " + ports)
        c.notice(self.channel, "commands: help, GOTO[n], RAND[n]")
        print "I'm in " + channel



        #server.privmsg(\"howi\", \"Hi there!\")

#        for nick in e.arguments()[2].split():
#           print nick

#            if nick[0] == "@":
#                nick = nick[1:]
#                self.channels[ch].set_mode("o", nick)
#            elif nick[0] == "+":
#                nick = nick[1:]
#                self.channels[ch].set_mode("v", nick)
#            self.channels[ch].add_user(nick)

    def delay_start_timer(self, c, e, bool):
        time.sleep(30)  # wait 30 seconds before starting auto-mode
        # if cmd == self.msg:
        self.run_timer(c, e, bool)

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) > 1 and irc_lower(a[0]) == irc_lower(self.connection.get_nickname()):
            cmd = a[1].strip()

            if cmd == "help":
                c.notice(self.channel, "\033[1;31mexample:\033[m")
                c.notice(self.channel, "x3340: #$$,...%")
                c.notice(self.channel, "\033[1;33mcommands:\033[m")
                c.notice(self.channel, "use space or _ for a rest")
                c.notice(self.channel, "use 'b' or 's' as first character to sync by bar or step")
                c.notice(self.channel, "if the string has 16 characters, they will be the same")
                c.notice(self.channel, "GOTO[n] ie. GOTO8, where n is a prime number (1-13)")
                c.notice(self.channel, "RAND[n] ie. RAND5, where n is a prime number (1-34)")
#               self.msg()

# cannot get threading >?
# need to be able to issue multiple commands (now no new commands get executed before the last one finished)

            # single random character array of size n
            elif cmd == "RAND1":
                self.make_rand_cmd(e, 1)
            elif cmd == "RAND2":
                self.make_rand_cmd(e, 2)
            elif cmd == "RAND3":
                self.make_rand_cmd(e, 3)
            elif cmd == "RAND5":
                self.make_rand_cmd(e, 5)
            elif cmd == "RAND8":
                self.make_rand_cmd(e, 8)
            elif cmd == "RAND13":
                self.make_rand_cmd(e, 13)
            elif cmd == "RAND21":
                self.make_rand_cmd(e, 21)
            elif cmd == "RAND34":
                self.make_rand_cmd(e, 34)

            # loop shuffling input runs n times
            elif cmd == "GOTO1":
                threading.Thread(target=self.run_timer(c, e, True, 1)).start()
            elif cmd == "GOTO2":
                threading.Thread(target=self.run_timer(c, e, True, 2)).start()
            elif cmd == "GOTO3":
                threading.Thread(target=self.run_timer(c, e, True, 3)).start()
            elif cmd == "GOTO5":
                threading.Thread(target=self.run_timer(c, e, True, 5)).start()
            elif cmd == "GOTO8":
                threading.Thread(target=self.run_timer(c, e, True, 8)).start()
            elif cmd == "GOTO13":
                threading.Thread(target=self.run_timer(c, e, True, 13)).start()
            else:
                threading.Thread(target=self.make_command(e, cmd)).start()

    def make_command(self, e, cmd):
        nick = nm_to_n(e.source())
        # debug regular user input
        c = self.connection
        c.notice(self.channel, cmd)
        self.send_osc_message(e, cmd)
        self.message = cmd
        #e.pubmsg("mode+v", "Hi there!")


    def make_rand_cmd(self, e, length=8, chars=string.letters + string.digits):

        nick = nm_to_n(e.source())
        c = self.connection
        #cmd = ''.join([random.choice(chars) for i in range(length)])
        #cmd = ''.join([random.choice(string.digits + ",.4%#  ") for i in range(length)])
        cmd = ''.join([random.choice(self.message) for i in range(length)])

        #self.message = cmd
        time.sleep(random.randrange(1,4))
        c.notice(self.channel, "-> " + nick + " " + cmd)
        self.send_osc_message(e, cmd)

    def send_osc_message(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
        msg = liblo.Message("/"+nick)

        if cmd != None:
            for i in cmd:
                msg.add(ord(i))
               
            for port in ports.split(','):
                liblo.send(targets[port], msg)
            
            print nick + ' -> ' + cmd


    def run_timer(self, c, e, bool, amount):
        nick = nm_to_n(e.source())
        count = 0
        g = "_"
        d = self.message
        print bool
        time.sleep(0.2)
        c.notice(self.channel, "<- " + nick + " " + d)
        time.sleep(random.randrange(1,4))
        while count < amount:
            msgsize = random.randrange(5,34)
            g = ''.join([random.choice(d) for i in range(msgsize)])
            #c.notice(self.channel, "-> " + g)
            c.notice(self.channel, "-> " + nick + " " + g)
            self.send_osc_message(e, g)
            time.sleep(random.randrange(3,8))
            count += 1
        else:
            print "timer stopped"

def main():
#    bot = R33401(channel, nickname, "irc.goto10.org", 6667)
    bot = R33401(channel, nickname, "127.0.0.1", 6667)
    bot.start()

if __name__ == "__main__":
    main()

