#!/usr/bin/env python
# craftivism.enyabot.py
# Originally written by Chris Saunders 
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make melodybot happy

import liblo, sys, r3340, re, random
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# melodybot config

channel = "#mode+v"
nickname = "enyaBot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
mybotsynth = "/bs4"

if len(sys.argv) == 2:
    server = sys.argv[1]

# melodybot's guts

class melody(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
	
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            nick = nm_to_n(e.source())
            if nick == "enya":
                self.do_command(e, a[0])
        return
    
    def on_privmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            cmd = a[0]
            if re.match("shut up", cmd):
                self.do_shutup()
            else:
                self.do_command(e, a[0])
    
    def do_command(self, e, cmd):
        msg = liblo.Message(self.mybotsynth)
        for i in cmd:
            if i == "_":
                msg.add(ord(i))
            else:
            	# blanksize = random.choice([4, 8, 8, 12, 12, 16, 24, 32])
            	msg.add(self.map_to_key(ord(i), 0, "minor") + 24)
            	#for i in range(0, blanksize):
	       	msg.add(95)
                msg.add(95)
                msg.add(95)
                msg.add(95)

		
	liblo.send(self.target, msg)
        c = self.connection
        c.notice(self.channel, "Enya's melody is " + cmd)
        r3340.notify(1, 'new melody:' + cmd)
        
    def do_shutup(self):
        msg = liblo.Message(self.mybotsynth)
        msg.add(ord("_"))
        liblo.send(self.target, msg)
        c = self.connection
        c.notice(self.channel, "I'm shutting up")
        r3340.notify(1, "Shutting up")
        
    def map_to_key(self, pitch, base, scale):
        if scale == "minor":
            map = [0, 2, 3, 5, 7, 8, 10, 12, 14, 15, 17, 19]
        elif scale == "major":
            map = [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19]
        oct = int(pitch/7)
        for i in range(0, len(map)):
            map[i] = map[i] + base
        remapped = map[pitch - (oct * 7)] + 60
        return remapped

# melodybot go!

melodybot = melody(channel, nickname, server, IRCport, OSCport, mybotsynth)
melodybot.start()
