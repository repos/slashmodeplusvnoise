#!/usr/bin/env python
# craftivism.bezBot.py
# Originally written by Chris Saunders
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340, random
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr


#### minibot config

nickname = "bezBot" 		# this bot's nickname
server = "irc.gosub10.org"
IRCport = 6667
channel = "#mode+v"

OSCport = 57120 		# sc's receiving port ( NetAddr.langPort )
mybotsynth = "/bs3" 		# sc botsynth's osctag


if len(sys.argv) == 2:
    server = sys.argv[1]

# minibot's guts
class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server
        c.notice(self.channel, "i'm mad for it me'")

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
        newMsg = ""

        mybotsynth = '/bs' + str(random.randint(1,6))

        msg = liblo.Message(mybotsynth)

	if random.randint(0,5) == 0:
            for i in cmd:
                new =  ord(i) + ( 12 * ( random.randint(0,4) - 2 ))
                newMsg += chr(new%256)
                msg.add(new)

            liblo.send(self.target, msg)
            c = self.connection
            c.notice(self.channel, newMsg)

            print cmd


# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
