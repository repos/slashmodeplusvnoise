#!/usr/bin/env python
# Originally written by ???
# DEPENDS: python-liblo, python-irclib, r3340

# modules required to make minibot happy

import liblo, sys, r3340, time, re
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# minibot config

channel = "#mode+v"
nickname = "fleabot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
mybotsynth = "/bs3"

if len(sys.argv) == 2:
    server = sys.argv[1]

# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        self.lastmsg = ''

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    '''
    def on_join(self, c, e):
        nick = nm_to_n(e.source())
        c.notice(self.channel, 'why hello there, ' + nick + '!')

    
    # QUIT HANDLERS ##########################################################
    def quit_handler(self, c, e):
        for chname, chobj in self.channels.items():
            users = chobj.users()
        numusers = len(users)
        c.notice(self.channel, 'someone just left... ' + str(numusers) + " users left in room.")

    def on_part(self, c, e):
        nick = nm_to_n(e.source())
        self.quit_handler(c, e)
        #c.notice(self.channel, nick + ' has just left the room.')

    def on_kick(self, c, e):
        nick = nm_to_n(e.source())
        self.quit_handler(c, e)
        #c.notice(self.channel, nick + ' has just been kicked out.')

    def on_quit(self, c, e):
        nick = nm_to_n(e.source())
        self.quit_handler(c, e)
        #c.notice(self.channel, nick + ' has quit.')

    # END QUIT HANDLERS ####################################################
    '''

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        if cmd == "" or cmd == " ":
            c.notice(self.channel, "Speak up, " + nick + "!")

        elif cmd == "fuck" or cmd == "shit" or cmd == "piss":
            c.notice(self.channel, "Hey, watch your language!")

        elif cmd == "flea shut up":
            r3340.notify(1, nick + " attempting to shut up.")
            msg = liblo.Message(self.mybotsynth)
            msg.add(ord("_"))
            liblo.send(self.target, msg)
            
        # master gets his message sent to OSC directly...
        elif nick == "flea":
            txtmsg = ""
            if cmd == "shut up":
                r3340.notify(1, nick + " attempting to shut up.")
                msg = liblo.Message(self.mybotsynth)
                msg.add(ord("_"))
                liblo.send(self.target, msg)
                self.do_shutup()
            elif cmd == "goflea":
                msg = liblo.Message(self.mybotsynth)
                for i in range(127):
                    msg.add(i)
                liblo.send(self.target, msg)
            elif cmd == "1":
                tune = "1___1___3___4_3_"
                msg = liblo.Message(self.mybotsynth)
                for i in tune:
                    if ord(i) != 95:
                        msg.add(self.map_to_key(((ord(i) % 24) + 48), 0, "minor"))
                        txtmsg += str(self.map_to_key(((ord(i) % 24) + 48), 0, "minor")) + ", "
                    else:
                        msg.add(ord("_"))
                        txtmsg += "95, "
                liblo.send(self.target, msg)
                r3340.notify(1, nick + ": " + txtmsg)
            elif cmd == "2":
                tune = "y__u__y__u__x_x_"
                msg = liblo.Message(self.mybotsynth)
                for i in tune:
                    if ord(i) != 95:
                        msg.add(self.map_to_key(((ord(i) % 24) + 48), 0, "minor"))
                        txtmsg += str(self.map_to_key(((ord(i) % 24) + 48), 0, "minor")) + ", "
                    else:
                        msg.add(ord("_"))
                        txtmsg += "95, "
                liblo.send(self.target, msg)
                r3340.notify(1, nick + ": " + txtmsg)
            elif cmd == "helpme":
                c.notice(self.channel, "You are in control of 'fleabot'. Fleabot makes bass sounds. Everything you type will be converted to numbers between 24 and 48. These should (might) correspond to notes C1 to C3. Maybe. Good luck!") ##################### THIS NEEDS TO BE SENT ONLY TO FLEA NOT TO EVERYONE
            elif cmd == "tetris":
                txtmsg = ''
                thearray = (52, 95, 47, 48, 50, 95, 48, 47, 45, 95, 45, 48, 52, 95, 50, 48, 47, 95, 47, 48, 50, 95, 52, 95, 48, 95, 45, 95, 45, 95, 95, 95)
                msg = liblo.Message(self.mybotsynth)
                for i in thearray:
                    msg.add(i)
                    txtmsg += str(i) + ", "
                liblo.send(self.target, msg)

            elif cmd.split(" ")[0] == "!f":
                r3340.notify(1, nick + ': changing filter frequency to ' + str(ord(cmd.split(" ")[1][0])))

            else:
                # No special message - manipulate string and send to supercollider
                msg = liblo.Message(self.mybotsynth)
                txtmsg = ""
                self.lastmsg = cmd
                for i in cmd:
                    if ord(i) != 95:
                        msg.add(self.map_to_key(((ord(i) % 24) + 48), 0, "minor"))
                        txtmsg += str(self.map_to_key(((ord(i) % 24) + 48), 0, "minor")) + ", "
                    else: #unless it's a rest, in which case leave it as a rest(
                        msg.add(ord("_"))
                        txtmsg += "95, "
                liblo.send(self.target, msg)
                r3340.notify(1, nick + ': ' + cmd + " [translation: " + str(txtmsg) + "]")

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)

    def do_shutup(self):
        msg = liblo.Message(self.mybotsynth)
        msg.add(ord("_"))
        liblo.send(self.target, msg)
        c = self.connection
        c.notice(self.channel, "I'm shutting up")
        r3340.notify(1, "Shutting up")

    def map_to_key(self, pitch, base, scale):
        if scale == "minor":
            map = [0, 2, 3, 5, 7, 8, 10, 12, 14, 15, 17, 19]
        elif scale == "major":
            map = [0, 2, 4, 5, 7, 9, 11, 12, 14, 16, 17, 19]
        oct = int(pitch/7)
        for i in range(0, len(map)):
            map[i] = map[i] + base
        remapped = map[pitch - (oct * 7)] + 30
        return remapped

# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
