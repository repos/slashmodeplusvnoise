#!/usr/bin/env python
# r3340 - an OSC IRC bot that want to take over the world
# DEPENDS: python-liblo, python-irclib

import liblo, sys, random, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# send to OSC server <port> on localhost
# joins <channel>, as <nickname>
if len(sys.argv) == 5:
    (nickname, server, channel, ports) = (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
else:
    print '\033[1;31mUsage:\033[m r3340.py <nickname> <server> \<irc-channel> <OSC port(s)>'
    print '\033[1;32mexample:\033[m ./r3340.py r3340 irc.goto10.org \#mode+v 57120'
    print '\033[1;32mexample:\033[m ./r3340.py banana localhost \#goto10 9997,9998,9999'
    sys.exit(1)

try:
    targets = {}
    for port in ports.split(','):
        targets[port] = liblo.Address(port)

except liblo.AddressError, err:
    print str(err)
    sys.exit("oioioi osc address error")

# synths
synths = {1 : "/a", 
          2 : "/b",
          3 : "/c", 
          4 : "/d"}


# connect to monolith
terminals = {1 : dec('192.168.1.103', 2010),
             2 : dec('192.168.1.103', 2011),
             3 : dec('192.168.1.103', 2012),
             4 : dec('192.168.1.103', 2013),
             5 : dec('192.168.1.103', 2006),
             6 : dec('192.168.1.103', 2007),
             7 : dec('192.168.1.103', 2008),
             8 : dec('192.168.1.103', 2009),
             9 : dec('192.168.1.103', 2002),
             10 : dec('192.168.1.103', 2003),
             11 : dec('192.168.1.103', 2004),
             12 : dec('192.168.1.103', 2005)}
for i in terminals:
    terminals[i].init()

# some init bullshit
y = 1
vt = 1
synth = 1

# we are the robots
class TestBot(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "I'm in " + channel

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        global synth
        synth = synth%4+1

        # random synth
        #msg = liblo.Message(random.choice(synths))
        # sequenced synth
        msg = liblo.Message(synths[synth])

        for i in cmd:
            msg.add(ord(i))
        for port in ports.split(','):
            liblo.send(targets[port], msg)

        global y
        global vt

        if y%24 +1 == 1 :
            terminals[vt].ansicmd("?5l")
            vt = vt%12 + 1
            terminals[vt].ansicmd("?5h")

        if y == 1 :
            for i in terminals:
                terminals[i].ansicmd("2J")

        terminals[vt].goto(1,y%24+1)
        terminals[vt].ansicmd("?K")
        terminals[vt].ansicmd("7m")
        terminals[vt].ansicmd("1m")
        terminals[vt].send(nick+": ",1,y%24+1)
        terminals[vt].ansicmd("0m")
        terminals[vt].send(" "+cmd,len(nick)+2,y%24+1)

        y = y%288 + 1

        print nick + ' -> ' + cmd

def main():
    bot = TestBot(channel, nickname, server, 6667)
    bot.start()

if __name__ == "__main__":
    main()
